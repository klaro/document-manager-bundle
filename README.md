Klaro Document Bundle
---------------

Bundle for handling document templates. Used in delta3 and saas-demo.

## Install via Composer

    composer require klaro/document-manager-bundle:dev-master

## Require in AppKernel.php

    new Klaro\DocumentManagerBundle\KlaroDocumentManagerBundle(),

## Add service

    imports:
      - { resource: '@KlaroDocumentManagerBundle/Resources/config/config.yml' }

## Add migration (if needed)

    <?php // app/DoctrineMigrations/Version20190909090909.php
    
    declare(strict_types=1);
    
    namespace KlaroCPQBundle\Migrations;
    
    use Klaro\DocumentManagerBundle\Migrations\BaseMigration;
    
    require_once __DIR__ . '/../../vendor/klaro/document-manager-bundle/DocumentManagerBundle/Migration/BaseMigration.php';
    
    final class Version20190909090909 extends BaseMigration
    {
        // add these two lines only to overwrite default table names
        const TABLE_GROUP = 'overwrited_document_group_table';
        const TABLE_TEMPLATE = 'overwrited_document_template_table';
        
        // define document groups
        const DOCUMENTS = [
            [
                'group_id' => 1,
                'identifier' => 'project_commercial_quotation',
                'name' => 'Commercial Quotation Template',
                'file_name' => 'project_commercial_quotation_#revision#.docx',
                // define init file (optional)
                'file' => __DIR__ . '/commercial_quotation_template.docx',
            ],
        ];
    }

## Configure

### Backend

    // config.yml
    klaro_document_manager:
        // to use another tables
        entities:
            group:      'overwrited_document_group_table'
            template:   'overwrited_document_template_table'
        // to not validate uploaded twig
        validation:
            twig:
                validate: false
        // to manage only few document groups (e.g. for sharing one table between projects)
        groups:
          - some_group
          - another_group

### Frontend

    // bower.json
    "jquery-file-upload": "~9.5.7",
    "jquery-ui": "jqueryui#~1.11.4",
    "moment": ">=2.10",
    "version.js": "^0.2.0"
    
    // index.html.twig
    {{ klaro_quotation_requirejs_config({
                ...
            'jquery-treetable'     : 'vendor/jquery-treetable/javascripts/src/jquery.treetable',
            'jquery.ui.widget'     : 'vendor/jquery-ui/ui/widget',
            'moment'               : 'vendor/moment/moment',
            'jqfileupload'         : 'vendor/jquery-file-upload/js/jquery.fileupload',
            'version.js'           : 'vendor/version.js/src/version',
            
Don't forget to make assets to create `web/bundles/klarodocumentmanager/js/adminTemplates.js`.

## Use in document builder

    // klarocpq_config.yml
    klaro_quotation:
      documents:
        commercial:
          type:               commercial
          title:              Commercial Quotation
          source:             'project_commercial_quotation'
              
    // services.yml
    project.document_builder.commercial:
        class: Project\Document\CommercialBuilder
        arguments:
          ...
          - '@klaro.document_manager'
        tags:
          - { name: 'klaro_quotation.document_builder', alias: 'commercial' }
          
    // CommercialBuilder.php
    $template = $this->documentManager->findLastTemplateByGroup($definition->getSource());
    $template->getFileContent(); // to get template content
