<?php

declare(strict_types=1);

namespace Klaro\DocumentManagerBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class BaseMigration extends AbstractMigration
{
    const TABLE_GROUP = 'klaro_document_group';
    const TABLE_TEMPLATE = 'klaro_document_template';

    const DOCUMENTS = [];

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(sprintf('CREATE TABLE `%s` (
                    `id` INT AUTO_INCREMENT NOT NULL,
                    `identifier` VARCHAR(255) NOT NULL,
                    `name` VARCHAR(255) NOT NULL,
                    `file_name` VARCHAR(255) NOT NULL,
                    PRIMARY KEY(`id`)
                ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB',
            static::TABLE_GROUP));

        $this->addSql(sprintf('CREATE TABLE `%s` (
                    `id` INT AUTO_INCREMENT NOT NULL,
                    `group_id` INT DEFAULT NULL,
                    `user_id` SMALLINT DEFAULT NULL,
                    `revision` SMALLINT DEFAULT NULL,
                    `last_modified` DATETIME DEFAULT NULL,
                    `file_content` LONGBLOB DEFAULT NULL,
                    INDEX IDX_%s (`group_id`), PRIMARY KEY(`id`)
                ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB',
            static::TABLE_TEMPLATE, md5(static::TABLE_TEMPLATE)));

        $this->addSql(sprintf('ALTER TABLE `%s` ADD CONSTRAINT `FK_%s` FOREIGN KEY (`group_id`) REFERENCES `%s` (`id`)',
            static::TABLE_TEMPLATE, md5(static::TABLE_GROUP . static::TABLE_TEMPLATE), static::TABLE_GROUP));

        foreach (static::DOCUMENTS as $document) {
            $this->addSql(sprintf('INSERT INTO `%s` (`id`, `identifier`, `name`, `file_name`)
                VALUES (%s, "%s", "%s", "%s")',
                static::TABLE_GROUP, $document['group_id'],  // TODO: be able to manage automatically
                $document['identifier'], $document['name'], $document['file_name']));

            if (array_key_exists('file', $document)) {
                $this->addSql(sprintf('INSERT INTO `%s` (`group_id`, `revision`, `file_content`)
                    VALUES (%s, 1, "%s")',
                    static::TABLE_TEMPLATE, $document['group_id'],  // TODO: be able to manage automatically
                    base64_encode(file_get_contents($document['file']))));
            }
        }
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // TODO: be able delete only if empty table
        $this->addSql(sprintf('DROP TABLE `%s`', static::TABLE_TEMPLATE));
        $this->addSql(sprintf('DROP TABLE `%s`', static::TABLE_GROUP));
    }
}
