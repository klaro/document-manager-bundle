require([
    'klaro_quotation', 'klaro_routing', 'jquery', 'underscore', 'moment',
    'jquery.ui.widget',
    'jqfileupload',
    'messenger'
], function (app, Routing, $, _, moment,
             jqwidget,
             jqfileupload,
             messengers) {
    "use strict";

    var messenger = new Messenger();
    var message   = null;

    // add table
    var compiled = _.template($('script#mainTable').html());
    $('div#mainTable').html(compiled({'documents': documents}));

    // bind main events: upload, data beautifier and etc
    var bindEvents = function() {
        // bind modal windows
        $('a.show_history').off('click');
        $('a.show_history').on('click', function handler() {
            var $el = $(this);

            // remove event from element to prevent doubleclick
            $el.off('click');

            // get group id
            var $id = $el.attr('data-groupid');

            // get list of files
            $.ajax({
                url: '?group_id=' + $id,
                dataType: 'json',
                success: function(data, textStatus, jqXHR){
                    // force remove to redraw
                    $('div#mainTable div#revisionListDialog').remove();

                    // create new
                    var compiled = _.template($('script#revisionListDialog').html());
                    var modal    = compiled({'label': data.name, 'documents': data.templates });
                    $('div#mainTable').append(modal);
                    $('div#revisionListDialog').modal('show');
                },
                complete: function(jqXHR, textStatus) {
                    // restore event after ajax done even with error
                    $el.on('click', handler);
                }
            });

            return false;
        });

        // bind nice-relative dates MomentJs
        $('td.moment').each(function() {
            var date = $(this).attr('data-date');

            if (date != '-') {
                $(this).html(moment(date, 'DD.MM.YYYY HH:mm:ss').fromNow());
            }
        });

        // jquery fileUpload
        $('input.fileupload').each(function() {
            var groupId  = $(this).attr('data-groupid');
            var revision = $(this).attr('data-revision');

            $(this).fileupload({
                url: '#',
                dataType: 'json',
                maxNumberOfFiles : 1,
                singleFileUploads: true,
                formData: {
                    'groupId'  : groupId,
                    'revision' : revision
                },
                add: function (e, data) {
                    data.submit();
                },
                start: function(e, data) {
                    // set uploading look
                    uploadStarted(this);
                },
                error: function(e, data) {
                    alert(e.responseText);
                },
                done: function(e, data) {
                    // update row with new template
                    var compiled   = _.template($('script#row').html());
                    $(this).closest("tr").replaceWith(compiled({'document': data.result}));

                    // rebind events
                    bindEvents();
                },
                always: function(e, data) {
                    // restore normal look
                    uploadFinished(this);
                }
            });
        });
    };

    /**
     * Modify look of upload button (uploading...)
     * @param el
     */
    var uploadStarted = function(el) {
        var $parent = $(el).parent();
        var $uploadIcon = $parent.find('.js-upload-icon'),
            $textHolder = $uploadIcon.next('span');

        $parent.addClass('disabled');
        $uploadIcon.attr('disabled', true).removeClass('icon-upload-alt').addClass('icon-spinner icon-spin');
        $textHolder.text('Uploading...');
    };

    /**
     * Modify look of upload button (uploading...)
     * @param el
     */
    var uploadFinished = function(el) {
        var $parent = $(el).parent();
        var $uploadIcon = $parent.find('.js-upload-icon'),
            $textHolder = $uploadIcon.next('span');

        $parent.removeClass('disabled');
        $uploadIcon.attr('disabled', false).removeClass('icon-spinner icon-spin').addClass('icon-upload-alt');
        $textHolder.text('Upload new version');
    };

    // Messenger informs if ajax has started
    $(document).ajaxStart(function () {
        message = messenger.post({
            type: 'info',
            message: 'Handling request.'
        });
    });

    // Messenger informs if ajax was a success
    $(document).ajaxSuccess(function () {
        message.update({
            type: 'success',
            message: 'Request complete.'
        });
    });

    // Messenger informs if ajax has returned an error
    $(document).ajaxError(function () {
        message.update({
            type: 'error',
            message: 'There was an error while handling the request. Please try again.'
        });
    });

    // start
    bindEvents();
});