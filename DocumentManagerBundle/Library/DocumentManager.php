<?php

namespace Klaro\DocumentManagerBundle\Library;

use Klaro\DocumentManagerBundle\Entity\DocumentGroupInterface;
use Klaro\DocumentManagerBundle\Entity\DocumentTemplate;
use Klaro\DocumentManagerBundle\Entity\DocumentTemplateInterface;
use Klaro\DocumentManagerBundle\Entity\DocumentTemplateRepositoryInterface;

use FOS\UserBundle\Model\User;
use Klaro\QuotationBundle\Api\AdminServicesInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment as Twig_Environment;
use Twig\Error\SyntaxError;

class DocumentManager implements AdminServicesInterface
{
    const NAME = 'Documents Templates';

    /** @var TwigEngine */
    protected $templating;

    /** @var Twig_Environment */
    protected $twig;

    /** @var ContainerInterface */
    protected $container;

    /** @var EntityManager */
    protected $em;

    /**
     * @param ContainerInterface $container
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ContainerInterface $container, ManagerRegistry $managerRegistry)
    {
        $this->container = $container;
        $this->templating = $container->get('templating');
        $this->em = $managerRegistry->getManager();
        $this->twig = $container->get('twig');

        $cm = $this->em->getClassMetadata('KlaroDocumentManagerBundle:DocumentGroup');
        $cm->setPrimaryTable(['name' => $container->getParameter('klaro_document_manager.entity.group')]);

        $cm = $this->em->getClassMetadata('KlaroDocumentManagerBundle:DocumentTemplate');
        $cm->setPrimaryTable(['name' => $container->getParameter('klaro_document_manager.entity.template')]);
    }

    /**
     * @return string
     */
    public static function getUserFriendlyName()
    {
        return static::NAME;
    }

    /**
     * Renders the whole page but the base twig is from the Quotation Bundle. Return renderView()
     *
     * @param Request $request
     * @return mixed
     */
    public function render(Request $request)
    {
        // Upload action
        if ($request->isMethod('POST')) {
            $this->uploadFile($request);
        } // Get list of files for history
        elseif ($request->isMethod('GET') && (int)$request->get('group_id')) {
            $this->getHistoryList($request);
        } // Download template
        elseif ($request->isMethod('GET') && (int)$request->get('template_id')) {
            $this->downloadFile($request);
        }

        return $this->renderAdminTemplatesTable();
    }

    /**
     * Download selected template
     *
     * @param Request $request
     */
    protected function downloadFile(Request $request)
    {
        /** @var DocumentTemplateInterface $template */
        $template = $this->em->find('KlaroDocumentManagerBundle:DocumentTemplate', $request->get('template_id'));

        if ($template) {
            /** @var DocumentGroupInterface $group */
            $group = $template->getGroupId();

            // Generate filename for download
            $fileName = $group->getFileName($template->getRevision());
            $content = $template->getFileContent();

            // Create proper headers for binary output
            $response = new Response();
            $response->headers->set('Content-type', 'application/octect-stream');
            $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $fileName));
            $response->headers->set('Content-Length', strlen($content));
            $response->headers->set('Content-Transfer-Encoding', 'binary');
            $response->setContent($content);
            $response->send();
            exit;
        }
    }

    /**
     * Build history list of existing files
     *
     * @param Request $request
     */
    protected function getHistoryList(Request $request)
    {
        $json = [];
        $groupId = $request->get('group_id', 0);

        /** @var DocumentGroupInterface $group */
        $group = $this->em->find('KlaroDocumentManagerBundle:DocumentGroup', $groupId);

        if ($group) {
            $json['name'] = $group->getName();

            $templateEntity = $this->em->getRepository('KlaroDocumentManagerBundle:DocumentTemplate');
            $templates = $templateEntity->findBy(['groupId' => $groupId], ['lastModified' => 'DESC']);

            // Build list of revision files
            if (count($templates)) {
                foreach ($templates as $template) {
                    $json['templates'][] = $this->prepareData($template, $group);
                }
            }
        }

        echo json_encode($json);
        exit;
    }

    /**
     * Upload file to DB
     *
     * @param Request $request
     */
    protected function uploadFile(Request $request)
    {
        $groupId = $request->get('groupId', 0);
        $revision = $request->get('revision', 0);
        $files = $request->files->get('files');

        // If we got group and files - upload them to db
        if ($groupId && count($files)) {
            // Only one template per upload
            $file = $files[0];

            /** @var UploadedFile $file */
            if ($file->isValid()) {
                /** @var DocumentGroupInterface $group */
                $group = $this->em->find('KlaroDocumentManagerBundle:DocumentGroup', $groupId);
                $securityContext = $this->container->get('security.token_storage');
                $user = $securityContext->getToken()->getUser();

                // Get binary stream
                $stream = fopen($file->getRealPath(), 'rb');

                // Perform additional validation of uploaded twig. Let us have Less problems in the future.
                if (substr($group->getFileName(), -5) === '.twig') {
                    if ($this->container->getParameter('klaro_document_manager.validation.twig')) {
                        $this->validateTwig($stream);
                    }
                }

                // Create new template
                $newTemplate = new DocumentTemplate();

                $newTemplate->setUserId($user);
                $newTemplate->setRevision(++$revision);
                $newTemplate->setGroupId($group);
                $newTemplate->setLastModified(new \DateTime());
                $newTemplate->setFileContent($stream);

                // Save to DB
                $this->em->persist($newTemplate);
                $this->em->flush();

                // Return results to update table row
                $json = $this->prepareData($newTemplate, $group);
                echo json_encode($json);
                exit;
            }
        }
    }

    /**
     * Validate twig
     *
     * @param $stream
     */
    protected function validateTwig($stream)
    {
        // Load content from binary stream
        $content = stream_get_contents($stream, -1, 0);

        try {
            $this->twig->parse($this->twig->tokenize($content));
        } catch (SyntaxError $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Build output array
     *
     * @param $template
     * @param $group
     * @return array
     */
    public function prepareData($template, $group)
    {
        $userName = '-';
        $revision = 0;
        $filename = $group->getFilename();
        $templateId = 0;
        $lastModified = '-';

        if (!empty($template)) {
            /** @var User $user */
            $user = $template->getUserId();

            // Get $userName for table
            if ($user) {
                $userName = $user->getFullName() ? $user->getFullName() : $user->getUsername();
                $userName = trim($userName);
            }

            // Prepare remaining fields
            $revision = $template->getRevision();
            $filename = $group->getFileName($revision);
            $templateId = $template->getId();
            $lastModified = $template->getLastModified() ? $template->getLastModified()->format('d.m.Y H:i:s') : '-';
        }

        $data = ['groupId' => $group->getId(),
            'name' => $group->getName(),
            'filename' => $filename,
            'revision' => $revision,
            'template_id' => $templateId,
            'lastModified' => $lastModified,
            'author' => $userName];

        return $data;
    }

    /**
     * Get all template groups and find for them last versions of templates
     *
     * @return mixed
     */
    public function renderAdminTemplatesTable()
    {
        /** @var DocumentGroupInterface[] $groups */
        /** @var DocumentTemplateRepositoryInterface $docRepo */
        $documents = [];

        if ($this->container->hasParameter('klaro_document_manager.groups')) {
            $groups = $this->em->getRepository('KlaroDocumentManagerBundle:DocumentGroup')->findBy([
                'identifier' => $this->container->getParameter('klaro_document_manager.groups')
            ]);
        } else {
            $groups = $this->em->getRepository('KlaroDocumentManagerBundle:DocumentGroup')->findAll();
        }
        $docRepo = $this->em->getRepository('KlaroDocumentManagerBundle:DocumentTemplate');

        usort($groups, function ($a, $b) {
            return $a->getId() > $b->getId();
        });

        foreach ($groups as $group) {
            $lastTemplate = $docRepo->findLastTemplateByGroupId($group->getId());
            $documents[] = $this->prepareData($lastTemplate, $group);
        }

        return $this->templating->render('KlaroDocumentManagerBundle::templates_list.html.twig', ['documents' => $documents]);
    }

    public function findLastTemplateByGroup($group)
    {
        if (is_numeric($group)) {
            $r = $this->em->getRepository('KlaroDocumentManagerBundle:DocumentTemplate')->findLastTemplateByGroupId($group);
        } else {
            $r = $this->em->getRepository('KlaroDocumentManagerBundle:DocumentTemplate')->getLastTemplateByGroupIdentifier($group);
        }

        return $r;
    }
}
