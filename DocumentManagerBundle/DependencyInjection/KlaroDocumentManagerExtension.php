<?php

namespace Klaro\DocumentManagerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class KlaroDocumentManagerExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('config.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('klaro_document_manager.entity.group', $config['entities']['group']);
        $container->setParameter('klaro_document_manager.entity.template', $config['entities']['template']);

        $container->setParameter('klaro_document_manager.validation.twig', $config['validation']['twig']['validate']);

        if (isset($config['groups']) && !empty($config['groups'])) {
            $container->setParameter('klaro_document_manager.groups', $config['groups']);
        }
    }
}
