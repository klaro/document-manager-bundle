<?php

namespace Klaro\DocumentManagerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class DocumentTemplateRepository extends EntityRepository implements DocumentTemplateRepositoryInterface
{
    /**
     * Find last template revision by group id
     *
     * @param $groupId
     * @return object|null
     */
    public function findLastTemplateByGroupId($groupId)
    {
        $result = null;

        $q = $this->createQueryBuilder('t');
        $q->andWhere($q->expr()->eq('t.groupId', $groupId));
        $q->orderBy('t.revision', 'DESC');
        $q->setMaxResults(1);
        $result = $q->getQuery()->getResult();

        // get one and first result
        if (!empty($result) && isset($result[0])) {
            $result = $result[0];
        }

        return $result;
    }

    /**
     * Find last template by group identifier
     *
     * @param $identifier
     * @return object|null
     */
    public function getLastTemplateByGroupIdentifier($identifier)
    {
        $result = null;
        $group = $this->_em->getRepository('KlaroDocumentManagerBundle:DocumentGroup')->findBy(['identifier' => $identifier]);

        if (count($group)) {
            /** @var DocumentGroup $group */
            $group = $group[0];
            $result = $this->findLastTemplateByGroupId($group->getId());
        }

        return $result;
    }
}
