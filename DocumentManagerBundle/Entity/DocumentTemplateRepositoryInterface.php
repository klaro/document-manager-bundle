<?php

namespace Klaro\DocumentManagerBundle\Entity;

interface DocumentTemplateRepositoryInterface
{
    /**
     * Find last template revision by group id
     *
     * @param $groupId
     * @return object|null
     */
    public function findLastTemplateByGroupId($groupId);

    /**
     * Find last template by group identifier
     *
     * @param $identifier
     * @return object|null
     */
    public function getLastTemplateByGroupIdentifier($identifier);
}