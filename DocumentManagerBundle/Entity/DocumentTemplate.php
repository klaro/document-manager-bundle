<?php

namespace Klaro\DocumentManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Klaro\DocumentManagerBundle\Entity\DocumentTemplateRepository")
 * @ORM\Table(name="klaro_document_template")
 */
class DocumentTemplate implements DocumentTemplateInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Klaro\DocumentManagerBundle\Entity\DocumentGroup", inversedBy="documentTemplates")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $groupId;

    /**
     * @ORM\ManyToOne(targetEntity="Klaro\Component\Common\Model\QuotationUserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $userId;

    /**
     * @ORM\Column(name="revision", type="smallint", nullable=true)
     */
    protected $revision;

    /**
     * @ORM\Column(name="last_modified", type="datetime", nullable=true)
     */
    protected $lastModified;

    /**
     * @ORM\Column(name="file_content", type="blob", nullable=true)
     */
    protected $fileContent;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * @param mixed $revision
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;
    }

    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @param mixed $lastModified
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;
    }

    /**
     * @return mixed
     */
    public function getFileContent()
    {
        // decode binary content from Blob
        return base64_decode(stream_get_contents($this->fileContent, -1, 0));
    }

    /**
     * @param resource $fileContent
     */
    public function setFileContent($fileContent)
    {
        $this->fileContent = base64_encode(stream_get_contents($fileContent, -1, 0));
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}
