<?php

namespace Klaro\DocumentManagerBundle\Entity;

interface DocumentTemplateInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getGroupId();

    /**
     * @param mixed $groupId
     */
    public function setGroupId($groupId);

    /**
     * @return mixed
     */
    public function getRevision();

    /**
     * @param mixed $revision
     */
    public function setRevision($revision);

    /**
     * @return mixed
     */
    public function getLastModified();

    /**
     * @param mixed $lastModified
     */
    public function setLastModified($lastModified);

    /**
     * @return mixed
     */
    public function getFileContent();

    /**
     * @param resource $fileContent
     */
    public function setFileContent($fileContent);

    /**
     * @return mixed
     */
    public function getUserId();

    /**
     * @param mixed $userId
     */
    public function setUserId($userId);
}
