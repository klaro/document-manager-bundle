<?php

namespace Klaro\DocumentManagerBundle\Entity;

interface DocumentGroupInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getIdentifier();

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier);

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param mixed $name
     */
    public function setName($name);

    /**
     * @param int|null $revision
     * @return mixed
     */
    public function getFileName($revision = null);

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName);

    /**
     * @return mixed
     */
    public function getDocumentTemplates();

    /**
     * @param mixed $documentTemplates
     */
    public function setDocumentTemplates($documentTemplates);
}
