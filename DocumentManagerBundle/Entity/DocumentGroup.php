<?php

namespace Klaro\DocumentManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="klaro_document_group")
 */
class DocumentGroup implements DocumentGroupInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="identifier", type="string", nullable=false)
     */
    protected $identifier;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="file_name", type="string", nullable=false)
     */
    protected $fileName;

    /**
     * @ORM\OneToMany(targetEntity="DocumentTemplate", mappedBy="groupId", cascade={"persist", "remove"})
     */
    protected $documentTemplates;

    public function __construct()
    {
        $this->documentTemplates = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param int|null $revision
     * @return mixed
     */
    public function getFileName($revision = null)
    {
        return is_null($revision) ? $this->fileName : str_replace('#revision#', $revision, $this->fileName);
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getDocumentTemplates()
    {
        return $this->documentTemplates;
    }

    /**
     * @param mixed $documentTemplates
     */
    public function setDocumentTemplates($documentTemplates)
    {
        $this->documentTemplates = $documentTemplates;
    }
}
