<?php

namespace Klaro\DocumentManagerBundle;

use Klaro\DocumentManagerBundle\DependencyInjection\KlaroDocumentManagerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KlaroDocumentManagerBundle extends Bundle
{
    public function getContainerExtension()
    {
        if ($this->extension === null) {
            $this->extension = new KlaroDocumentManagerExtension();
        }

        return $this->extension;
    }
}
